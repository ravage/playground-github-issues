using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;
using System;
using System.Diagnostics;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class IssuesController : Controller
    {
        private readonly IMemoryCache cache;
        private readonly ILogger logger;

        public IssuesController(IMemoryCache cache, ILogger<IssuesController> logger)
        {
            this.cache = cache;
            this.logger = logger;
        }

        [HttpGet]
        public string Get(string repository)
        {
            var client = new App.Client();
            var json = "{}";

            if (cache.TryGetValue(repository, out json)) {
                logger.LogInformation("Cache Hit");
            } else {
                logger.LogInformation("Cache Miss");
                json = client.IssuesFor(repository);
                cache.Set(repository, json, TimeSpan.FromMinutes(30));
            }

            var payload = JsonConvert.DeserializeObject<IEnumerable<App.Issue>>(json);
            return JsonConvert.SerializeObject(payload);
        }
    }
}
