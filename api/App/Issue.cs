using Newtonsoft.Json;
using System;

namespace api.App
{
    public class Issue
    {
        [JsonConstructor]
        public Issue(string url, string title, string html_url, DateTime created_at, DateTime updated_at) {
            Url = url;
            Title = title;
            HtmlUrl = html_url;
            CreatedAt = created_at;
            UpdatedAt = updated_at;
        }

        public string Url { get; private set; }        
        public string Title { get; private set; }
        public string HtmlUrl { get; private set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}