using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace api.App
{
    public class Client
    {
        private const string host = "https://api.github.com";
        private async Task<string> Get(string uri)
        {
            var endpoint = string.Format("{0}/{1}", host, uri);
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", "HASLab Client");
            var stringTask = client.GetStringAsync(endpoint);

            return await stringTask;
        }

        public string IssuesFor(string repository) {
            return Get(string.Format("repos/{0}/issues", repository)).Result;
        }
    }
}