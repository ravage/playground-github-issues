using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace frontend.App
{
    public class Issue
    {
       public string Url { get; set; }
        public string Title { get; set; }
        public string Body {get; set; }
        public string HtmlUrl { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}