﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace frontend.Controllers
{
    public class IssuesController : Controller
    {
        [HttpPost]
        public IActionResult Index(string repository)
        {
            var host = "issues-backend"; //Environment.GetEnvironmentVariable("ISSUES_BACKEND_SERVICE_HOST");
            var port = Environment.GetEnvironmentVariable("ISSUES_BACKEND_SERVICE_PORT");
            var endpoint = string.Format($"http://{host}:{port}/api/issues/?repository={repository}");
            var client = new HttpClient();
            var payload = client.GetStringAsync(endpoint);
            var data = JsonConvert.DeserializeObject<List<App.Issue>>(payload.Result);
            return View("Issues", data);
        }

        public IActionResult Index()
        {
            return View("Index");
        }
    }
}
