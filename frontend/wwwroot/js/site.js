﻿(function($) {
    var responseEl = $('.response');
    console.log("Init!")

    $("form").submit(function(ev) {
        post(this);
        ev.preventDefault()
    });

    function post(el) {
        $.ajax({
            type: "POST",
            url: "/",
            data: $(el).serialize()
        })
        .done(function(ev) {
            responseEl.html(ev);
        })
        .fail(function(ev) {
            console.log(ev)
        })
    }
    
})(jQuery)
